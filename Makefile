TARGET ?= ~/.local

.PHONY: install

install:
	stow -t $(TARGET) -R src
